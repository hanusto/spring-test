package com.example;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.example.model.Person;
import com.example.service.PersonService;

public class PersonServiceIntegrationTest {
	@Autowired
	private PersonService personService;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Test
	public void shouldCreateNewPerson() {
		Person person = new Person();
		person.setFirstName("Kenan");
		person.setLastName("Sevindik");
		
		long countBeforeInsert = jdbcTemplate.queryForObject("select count(*) from t_person", Long.class);
		Assert.assertEquals(2, countBeforeInsert);
		
		personService.create(person);
		
		long countAfterInsert = jdbcTemplate.queryForObject("select count(*) from t_person", Long.class);
		Assert.assertEquals(3, countAfterInsert);
	}
	
	@Test
	public void shouldDeleteNewPerson() {
		long countBeforeDelete = jdbcTemplate.queryForObject("select count(*) from t_person", Long.class);
		Assert.assertEquals(2, countBeforeDelete);
		
		personService.delete(1L);
		
		long countAfterDelete = jdbcTemplate.queryForObject("select count(*) from t_person", Long.class);
		Assert.assertEquals(1, countAfterDelete);
	}
	
	@Test
	public void shouldFindPersonsById() {
		Person person = personService.findById(1L);

		assertThat(person, notNullValue());
		assertThat("John", is(person.getFirstName()));
		assertThat("Doe", is(person.getLastName()));
	}
	
	@Test
	public void shouldFindPersonsByName() {
		assertThat(personService.findByName("Hanus").getFirstName(), is("Tomas"));
	}

	@Test
	public void testAspect() {
		assertThat(personService.aspect("test"), is("test"));
	}
}
